# Backend

## Dependencies

- Python3
- MySQL

To install the required python packages, you can run:

```
pip3 install -r requirements.txt
```

## Create Database

Before running the script, you must initialize a database named `soliditycpn`.

```
  - cd scripts
  - python3 createDatabase.py
  - python3 createDatabase2.py
```

# Run project

## Make database migrations

```
  - python3 manage.py makemigrations
  - python3 manage.py migrate
```

## Run

```
  - python3 manage.py runserver 8000
```

# Update Tools

Copy unfolding tools (just the executable with the name `unfolding`) to the
`backend/tools/tools` directory.
