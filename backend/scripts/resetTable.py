import os

import mysql.connector

db = mysql.connector.connect(
    host=os.getenv("DB_HOST", "127.0.0.1"),
    port=os.getenv("DB_PORT", "3306"),
    user=os.getenv("DB_USER", "root"),
    passwd=os.getenv("DB_PWD", "123456"),
    database=os.getenv("DB_NAME", "soliditycpn"),
)
mycursor = db.cursor()

# CheckedBatchSC table
mycursor.execute("DROP TABLE CheckedBatchSC")

# VulnerabilitySetting table
mycursor.execute("DROP TABLE VulnerabilitySetting")


# CPNContext table
mycursor.execute("DROP TABLE CPNContext")


# CheckedSmartContractDetail table
mycursor.execute("DROP TABLE CheckedSmartContractDetail")


# LNAFile table
mycursor.execute("DROP TABLE LNAFile")


# InitialMarking table
mycursor.execute("DROP TABLE InitialMarking")


# Balance table
mycursor.execute("DROP TABLE Balance")


# IMFunction table
mycursor.execute("DROP TABLE INFunction")


# IMArgument table
mycursor.execute("DROP TABLE IMArgument")
