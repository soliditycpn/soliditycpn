from django.urls import path

from .views import CheckCpnModelAPIView, GenerateCpnModelAPIView

urlpatterns = [
    path(
        "generate-cpn-model",
        GenerateCpnModelAPIView.as_view(),
        name="generate_cpn_model",
    ),
    path("check-cpn-model", CheckCpnModelAPIView.as_view(), name="check_cpn_model"),
]
