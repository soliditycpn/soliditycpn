import json
import os
import subprocess


def removeFolder(full_path):
    try:
        if os.path.exists(full_path):
            for file_path in os.listdir(full_path):
                os.remove(full_path + file_path)
            os.rmdir(full_path)
    except:
        raise Exception(f"Error while removing folder {full_path}")


def saveFileToTemp(folder_path, file_name, file_content, dtype="text"):
    try:
        if dtype == "text":
            with open(folder_path + file_name, "a") as f:
                f.write(file_content)
        elif dtype == "json":
            with open(folder_path + file_name, "a") as f:
                json.dump(file_content, f)
    except:
        raise Exception(f"Error while saving {file_name} in temporal folder")


def readFile(file_path):
    with open(file_path, "r") as f:
        return f.read()


def unfolding(
    folder_path,
    sol_ast_name,
    lna_name,
    lna_json_name,
    context_name,
    context_type,
    ltl_name,
    initial_marking,
):
    lna_PATH = os.path.join(folder_path, lna_name)
    context_PATH = os.path.join(folder_path, context_name)
    ltl_PATH = os.path.join(folder_path, ltl_name)
    sol_ast_PATH = os.path.join(folder_path, sol_ast_name)
    lna_json_PATH = os.path.join(folder_path, lna_json_name)
    initial_marking_PATH = os.path.join(folder_path, initial_marking)

    output_PATH = folder_path
    output_NAME = lna_name.split(".")[0] + "_HCPN"

    BINARY_FOLDER = "./tools/tools/unfolding/bin"

    command = (
        os.path.join(BINARY_FOLDER, "unfolding")
        + f" --sol-ast {sol_ast_PATH}"
        + f" --lna {lna_PATH}"
        + f" --lna-info {lna_json_PATH}"
        + f" --im {initial_marking_PATH}"
        + f" --context-type {context_type}"
        + f" --context {context_PATH}"
        + f" --ltl {ltl_PATH}"
        + f" --output-path {output_PATH}"
        + f" --output-name {output_NAME}"
    )

    output = subprocess.run(
        command, cwd="./", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )

    return {
        "err": output.stderr.decode(),
        "folder": output_PATH,
        "context": output_NAME + ".context.lna",
        "hcpn": output_NAME + ".lna",
        "prop": output_NAME + ".prop.lna",
    }


def generate_ast_files(folder_path, sol_name):
    sol_PATH = os.path.join(folder_path, sol_name)

    BINARY_FOLDER = "./tools/tools/unfolding/bin"
    command = (
        os.path.join(BINARY_FOLDER, "solc-linux")
        + " --ast --ast-compact-json"
        + f" -o {folder_path}"
        + f" {sol_PATH}"
    )
    output = subprocess.run(
        command, cwd="./", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )

    return {
        "err": output.stderr.decode(),
        "folder": folder_path,
        "ast": f"{sol_name}.ast",
        "ast_compact": f"{sol_name}_json.ast",
    }


def helenaCheck(folder_path, hcpn_name, prop_name):
    hcpn_PATH = folder_path + hcpn_name
    prop_PATH = folder_path + prop_name

    prop_content = readFile(prop_PATH)

    begin = prop_content.find("property") + 9
    end = prop_content.find(":")

    property_name = prop_content[begin:end]

    command = (
        "helena -N=CHECK --hash-size=26 -P=1" + " -p=" + property_name + " " + hcpn_PATH
    )

    output = subprocess.run(
        command, cwd="./", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )

    return {"err": output.stderr.decode(), "output": output.stdout.decode()}
