import os

from mainfolder.authentication import JSONWebTokenAuthentication
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .functionals import (generate_ast_files, helenaCheck, readFile,
                          removeFolder, saveFileToTemp, unfolding)


class GenerateCpnModelAPIView(APIView):
    authentication_classes = []

    def post(self, request):
        data = request.data
        user = request.user

        user_id = "anonym" if user.is_anonymous else str(user.aid)

        # create output folder
        current_path = "./tools/temp/"
        full_path = current_path + user_id + "_generate" + "/"
        try:
            if os.path.exists(full_path):
                removeFolder(full_path)
            os.makedirs(full_path)
        except OSError:
            removeFolder(full_path)
            return Response(
                {"detail": "Generate folder error!"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        # Get content of the smart contract from the frontend
        sc = data["selected-smartcontract"][0]
        sc_name = sc["name"] + ".sol"
        sc_content = sc["content"]

        # Get information about the vulnerability from the frontend
        vulnerability_name = "vulnerability.json"
        vulnerability = data["selected-vulnerability"]

        # Get information about the initial marking from the frontend
        initial_marking_name = "initial_marking.json"
        initial_marking = data["initial-marking"]

        # Get information about the behavioral context from the frontend
        context = data["selected-context"]
        context_name = context["data"]["name"]
        context_content = context["data"]["content"]
        context_type = context["data"]["context_type"]
        if context_type == "DCR":
            context_name += ".xml"
        elif context_type == "CPN":
            context_name += ".lna"
        else:
            context_name += ".txt"

        # saving smart contract, context, vulnerability and initial_marking in temporal files
        try:
            saveFileToTemp(full_path, sc_name, sc_content, dtype="text")
            saveFileToTemp(full_path, context_name, context_content, dtype="text")
            saveFileToTemp(full_path, vulnerability_name, vulnerability, dtype="json")
            saveFileToTemp(
                full_path, initial_marking_name, initial_marking, dtype="json"
            )
        except Exception as e:
            removeFolder(full_path)
            return Response(
                {"detail": str(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        # generate the ast files from the solidity compiler
        ast_files = generate_ast_files(full_path, sc_name)

        # TODO: generate CPN from smart contract and the info file (json)
        unfolder_path = "./tools/tools/unfolding/test/etherGame"
        lna_name = "etherGame.lna"
        lna_info_name = "etherGame.json"
        lna_source = os.path.join(unfolder_path, lna_name)
        lna_info_source = os.path.join(unfolder_path, lna_info_name)
        os.system(f'cp {lna_source} {full_path}')
        os.system(f'cp {lna_info_source} {full_path}')

        # call the unfolding tool
        output = unfolding(
            full_path,
            ast_files["ast"],
            lna_name, # TODO: generate CPN of the smart contract
            lna_info_name,   # TODO: generate the JSON of the CPN
            context_name,
            context_type,
            vulnerability_name,
            initial_marking_name,
        )

        if len(output["err"]) > 0:
            removeFolder(full_path)
            return Response(
                {"detail": output["err"]}, status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

        # return the output
        output_folder = output["folder"]
        try:
            context_content = readFile(output_folder + output["context"])
            hcpn_content = readFile(output_folder + output["hcpn"])
            prop_content = readFile(output_folder + output["prop"])

            removeFolder(full_path)
            return Response(
                {
                    "hcpn": {
                        "name": output["hcpn"],
                        "content": hcpn_content,
                    },
                    "prop": {
                        "name": output["prop"],
                        "content": prop_content,
                    },
                    "context": {
                        "name": context_name,
                        "content": context_content,
                        # TODO: check this
                        "context_type": "CPN",
                    },
                },
                status=status.HTTP_200_OK,
            )
        except:
            removeFolder(full_path)
            return Response(
                {"detail": "Generate file error!"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class CheckCpnModelAPIView(APIView):
    authentication_classes = []

    def post(self, request):
        data = request.data
        user = request.user

        user_id = "anonym" if user.is_anonymous else str(user.aid)
        hcpn_data = data["hcpn-data"]

        current_path = "./tools/temp/"
        full_path = current_path + user_id + "_checking" + "/"
        try:
            if os.path.exists(full_path):
                removeFolder(full_path)
            os.makedirs(full_path)
        except OSError:
            removeFolder(full_path)
            return Response(
                {"detail": "Generate folder error!"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        # saving hcpn and property in temporal files
        hcpn_name = hcpn_data["hcpn"]["name"]
        hcpn_content = hcpn_data["hcpn"]["content"]
        prop_name = hcpn_data["prop"]["name"]
        prop_content = hcpn_data["prop"]["content"]
        try:
            saveFileToTemp(
                full_path,
                hcpn_name,
                hcpn_content,
                dtype="text",
            )
            saveFileToTemp(
                full_path,
                prop_name,
                prop_content,
                dtype="text",
            )
        except Exception as e:
            removeFolder(full_path)
            return Response(
                {"detail": str(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        # call helena model-checker
        output = helenaCheck(full_path, hcpn_name, prop_name)

        start = output["output"].find("Helena report")
        if start > 0:
            result = output["output"][start:]
            removeFolder(full_path)
            return Response(result, status=status.HTTP_200_OK)
        else:
            removeFolder(full_path)
            return Response(
                {"detail": output},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
