#!/bin/bash
#
# installation script for helena ubuntu machines
#
# author: Jaime Arias (https://github.com/himito)

# Installation Folder
INSTALL_FOLDER="${HOME}"

# install dependencies
apt-get update
apt-get install -y make bc git gcc gprbuild gnat python gprbuild

# clone helena
cd "${INSTALL_FOLDER}"
git clone https://github.com/sami-evangelista/helena.git helena && cd helena/src

# fix
mkdir -p "lna/obj"

# compile helena
./compile-helena --clean-all
./compile-helena --with-ltl

# add binaries to the /usr/local/bin folder
cd ../bin && cp * /usr/local/bin

# remove helena folder
cd ../../ && rm -rf helena*
