from django.urls import path

from .views import GetCheckedBatchAPIView, SaveCheckedBatchAPIView

urlpatterns = [
    path(
        "save-checked-batch",
        SaveCheckedBatchAPIView.as_view(),
        name="save_checked_batch",
    ),
    path(
        "get-checked-batch", GetCheckedBatchAPIView.as_view(), name="get_checked_batch"
    ),
]
