from django.urls import path

from .views import CrudSmartContract, GetAllAPIView, GetInforAPIView

urlpatterns = [
    path("get_all", GetAllAPIView.as_view(), name="getall"),
    path("get_vfinfor", GetInforAPIView.as_view(), name="get_vfinfor"),
    path("crud-sc", CrudSmartContract.as_view(), name="create_sc"),
]
