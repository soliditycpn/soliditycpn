from django.urls import path

from .views import ContextAPIView, GetAllContextAPIView

urlpatterns = [
    path("get-all", GetAllContextAPIView.as_view(), name="getall"),
    path("cpn-context-crud", ContextAPIView.as_view()),
]
