from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from .models import Account


class LoginSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=200)
    username = serializers.CharField(max_length=200)


class RegisterSerializer(serializers.Serializer):
    aid = serializers.CharField(max_length=64)
    username = serializers.CharField(
        max_length=200,
        validators=[
            UniqueValidator(
                queryset=Account.objects.all(), message=("User already exists")
            )
        ],
    )
    password = serializers.CharField(max_length=200)

    def create(self, validated_data):
        return Account.objects.create(**validated_data)
