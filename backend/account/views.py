import uuid

from mainfolder.authentication import JSONWebTokenAuthentication
from rest_framework import exceptions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Account, Contact
from .serializers import LoginSerializer, RegisterSerializer


class LoginAPIView(APIView):
    authentication_classes = []

    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            username = serializer.data["username"]
            password = serializer.data["password"]
            try:
                user = Account.objects.get(username=username)
                if password != user.password:
                    raise exceptions.AuthenticationFailed(
                        {"message": "Incorrect password!", "target": "password"}
                    )

                return Response(
                    {
                        "accessToken": JSONWebTokenAuthentication.generate_jwt(
                            username, "access", 1
                        ),
                        "refreshToken": JSONWebTokenAuthentication.generate_jwt(
                            username, "refresh", 24
                        ),
                    },
                    status=status.HTTP_200_OK,
                )
            except Account.DoesNotExist:
                raise exceptions.AuthenticationFailed(
                    {"message": "User not found!", "target": "user"}
                )

        else:
            return Response(
                {"detail": {"message": "Login parameters not correct!!!"}},
                status=status.HTTP_400_BAD_REQUEST,
            )


class CurrentAPIView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]

    def get(self, request):
        user = request.user
        try:
            contact = Contact.objects.get(aid=user.aid)
            return Response(
                {
                    "id": user.aid,
                    "username": user.username,
                    "role": user.role,
                    "firstname": contact.firstname,
                    "lastname": contact.lastname,
                    "email": contact.email,
                },
                status=status.HTTP_200_OK,
            )
        except Contact.DoesNotExist:
            return Response(
                {
                    "id": user.aid,
                    "username": user.username,
                    "role": user.role,
                    "firstname": "",
                    "lastname": "",
                    "email": "",
                },
                status=status.HTTP_200_OK,
            )


class RefreshTokenAPIView(APIView):
    authentication_classes = []

    def post(self, request):
        username = request.data["username"]
        return Response(
            {
                "accessToken": JSONWebTokenAuthentication.generate_jwt(
                    username, "access", 1
                )
            }
        )


class RegisterAPIView(APIView):
    authentication_classes = []

    def post(self, request):
        form = {"aid": str(uuid.uuid4()), **request.data}
        serializer = RegisterSerializer(data=form)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                return Response(
                    {"detail": {"message": "User successfully created"}},
                    status=status.HTTP_200_OK,
                )
        return Response(
            {"detail": serializer.errors},
            status=status.HTTP_400_BAD_REQUEST,
        )
