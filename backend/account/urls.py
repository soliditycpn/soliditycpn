from django.urls import path

from .views import CurrentAPIView, LoginAPIView, RefreshTokenAPIView, RegisterAPIView

urlpatterns = [
    path("login", LoginAPIView.as_view(), name="login"),
    path("register", RegisterAPIView.as_view(), name="register"),
    path("current", CurrentAPIView.as_view(), name="current"),
    path("refresh-tokens", RefreshTokenAPIView.as_view(), name="refresh"),
]
