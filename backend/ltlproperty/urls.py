from django.urls import path

from .views import GetAllAPIView, GetLTLTemplateAPIView

urlpatterns = [
    path("get-all", GetAllAPIView.as_view(), name="getall"),
    path("get-ltltemplate", GetLTLTemplateAPIView.as_view(), name="generate_cpn_model"),
]
