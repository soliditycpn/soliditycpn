module.exports = {
  devServer: {
    proxy: process.env.APP_BASE_URL || "/",
    allowedHosts: [
      "localhost",
      "soliditycpn.lipn.univ-paris13.fr",
      "192.168.95.20",
    ],
  },

  transpileDependencies: ["vuetify"],
};
