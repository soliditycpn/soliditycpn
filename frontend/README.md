# Solidity2CPN Frontend

## Dependencies

- nodejs >= 16

## Project setup

```
npm install
```

## Compiles and hot-reloads for development

```
npm run serve
```

## Project structure

```
.
└── src                             # folder with the sources of the project
    ├── assets                      # folder with the images
    ├── components                  # folder with the vue components
    ├── mixins
    ├── router                      # folder with the routes and middlewares of the app
    ├── services                    # folder with the API interface and user management
    ├── solidity
    ├── store                       # folder with the vuex store (data management)
    └── views                       # folder with the views of the app
```
