import sha256 from "crypto-js/sha256";

export default {
  methods: {
    hashValue: function (value) {
      return sha256(value).toString();
    },
  },
};
