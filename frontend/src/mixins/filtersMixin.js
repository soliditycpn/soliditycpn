import { format } from "date-fns";

export default {
  filters: {
    dateFormat(value) {
      if (!value) return "";
      return format(new Date(value), "dd/MM/yyyy HH:mm");
    },
  },
};
