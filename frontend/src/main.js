import Vue from "vue";
import App from "@/App.vue";
import titleMixin from "@/mixins/titleMixin";
import filterMixin from "@/mixins/filtersMixin";
import hashMixin from "@/mixins/hashMixin";
import router from "@/router";
import store from "@/store";
import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;

// mixins
Vue.mixin(hashMixin);
Vue.mixin(titleMixin);
Vue.mixin(filterMixin);

new Vue({
  store,
  router,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
