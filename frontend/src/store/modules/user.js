import { UsersService } from "@/services/users.js";

const state = {
  currentUser: {
    id: "",
    username: "",
    role: "",
    firstname: "",
    lastname: "",
    email: "",
  },
};

const mutations = {
  SET_CURRENT_USER(state, currentUserData) {
    state.currentUser = currentUserData;
  },
};

const actions = {
  getCurrent({ commit }) {
    return UsersService.getCurrent()
      .then((user) => commit("SET_CURRENT_USER", user.data))
      .catch((error) => {
        error;
      });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
