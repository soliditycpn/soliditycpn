import Vue from "vue";
import Vuex from "vuex";
import modules from "./modules";
import { DOMAIN_TITLE } from "@/config";
Vue.use(Vuex);

const state = {
  title: DOMAIN_TITLE,
};

const getters = {
  title: (state) => state.title,
};

export default new Vuex.Store({
  modules,
  state,
  getters,
});
