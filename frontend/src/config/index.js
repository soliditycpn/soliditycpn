module.exports = {
  API_URL: process.env.VUE_APP_API_URL || "http://127.0.0.1:8000",
  DOMAIN_TITLE: process.env.VUE_APP_DOMAIN_TITLE || "Solidity2CPN",
};
