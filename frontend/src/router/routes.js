import Index from "@/views/Index.vue";

// views related to authentication
import Login from "@/views/Account/Login.vue";
import Register from "@/views/Account/Register.vue";

// views related to smart contracts
import ListSC from "@/views/SmartContract/ListSmartContracts.vue";
import AddSC from "@/views/SmartContract/AddSmartContract.vue";
import EditSC from "@/views/SmartContract/EditSmartContract.vue";

// views related to context
import ListContexts from "@/views/Context/ListContexts.vue";
import AddContext from "@/views/Context/AddContext.vue";
import EditContext from "@/views/Context/EditContext.vue";

// views related to ltl
import ListProperties from "@/views/Property/ListProperties.vue";
import EditProperty from "@/views/Property/EditProperty.vue";
import AddProperty from "@/views/Property/AddProperty.vue";

// views related to check vulnerabilities
import Roadmap from "@/views/CheckVulnerabilities/RoadMap.vue";

export const routes = [
  {
    path: "/",
    name: "Index",
    component: Index,
    meta: { requiresAuth: false },
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: { requiresAuth: false },
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
    meta: { requiresAuth: false },
  },
  /** Smart Contracts CRUD */
  {
    path: "/list-sc",
    name: "ListSC",
    component: ListSC,
    meta: { requiresAuth: false },
  },
  {
    path: "/add-sc",
    name: "AddSC",
    component: AddSC,
    meta: { requiresAuth: false },
  },
  {
    path: "/edit-sc",
    name: "EditSc",
    component: EditSC,
    meta: { requiresAuth: false },
  },
  /** Context CRUD */
  {
    path: "/list-context",
    name: "ListContext",
    component: ListContexts,
    meta: { requiresAuth: false },
  },
  {
    path: "/add-context",
    name: "AddContext",
    component: AddContext,
    meta: { requiresAuth: false },
  },
  {
    path: "/edit-context",
    name: "EditContext",
    component: EditContext,
    meta: { requiresAuth: false },
  },
  /** LTL CRUD */
  {
    path: "/list-ltl",
    name: "ListLTL",
    component: ListProperties,
    meta: { requiresAuth: false },
  },
  {
    path: "/add-ltl",
    name: "AddLTL",
    component: AddProperty,
    meta: { requiresAuth: false },
  },
  {
    path: "/edit-ltl",
    name: "EditLTL",
    component: EditProperty,
    meta: { requiresAuth: false },
  },
  /** Check Vulnerabilities */
  {
    path: "/check-vulnerabilities/",
    name: "CheckVulnerabilities",
    component: Roadmap,
    meta: { requiresAuth: false },
  },
];
