import Vue from "vue";
import VueRouter from "vue-router";

import $store from "@/store";
import { routes } from "./routes";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

/**
 * Check if user has access to the page. Otherwise, redirect to login page
 * TODO: What is the difference between  meta.guest and !meta.requiresAuth?
 */
router.beforeEach((to, from, next) => {
  const currentUserId = $store.state.user.currentUser.id;

  // check for auth route
  const isAuthRoute = to.matched.some((record) => record.meta.requiresAuth);
  if (isAuthRoute) {
    return currentUserId ? next() : next({ name: "Login" });
  }

  // check for guest route
  const isGuestRoute = to.matched.some((record) => record.meta.guest);
  if (isGuestRoute) {
    return currentUserId ? next({ name: "Index" }) : next();
  }

  return next();
});

export default router;
