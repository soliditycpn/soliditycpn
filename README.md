# Project structure

## Download

```
git clone --recursive https://depot.lipn.univ-paris13.fr/soliditycpn/soliditycpn.git
```

## Backend

Contain backend source codes (`python - Django`)

## Frontend

Contain front-end source codes (`javascript - VueJS`)

## Tools

Contain all tools to process core businesses (`C/C++, Helena`)

The included files need to be compiled before pulling down in new machine.

Compile:

```
  - cd tools/include
  - g++ -c ./*.cpp -std=c++11
```

## Development

For development, a development environment can be created with Docker as follows:

```
> docker compose up
```

## Production Environment

For `production` is important to create a `.env` file with the configuration.

```
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
```
